package com.svvdev.trainingonrestapi.cards;

import android.support.v7.widget.CardView;


public interface CardFragment {

    public CardView getCardView();

}
