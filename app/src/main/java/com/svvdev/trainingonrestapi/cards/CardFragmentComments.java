package com.svvdev.trainingonrestapi.cards;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.svvdev.trainingonrestapi.utils.adapters.CardAdapter;
import com.svvdev.trainingonrestapi.R;
import com.svvdev.trainingonrestapi.resclasses.Comment;
import com.svvdev.trainingonrestapi.utils.ApiService;
import com.svvdev.trainingonrestapi.utils.InternetConnection;
import com.svvdev.trainingonrestapi.utils.RetroClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CardFragmentComments extends Fragment implements CardFragment {

    private CardView cardView;
    private TextView commentCardviewGuideText;
    private TextView commentHeader;
    private TextView commentBody;
    private TextView commentAuthor;

    public static Fragment getInstance(int position){
        CardFragmentComments f = new CardFragmentComments();
        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);

        return f;
    }

    @SuppressLint("DefaultLocale")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.item_viewpager_comments, container, false);

        cardView = (CardView) view.findViewById(R.id.commentsCardView);
        cardView.setMaxCardElevation(cardView.getCardElevation() * CardAdapter.MAX_ELEVATION_FACTOR);

        Button commentApplyButton = (Button)view.findViewById(R.id.commentsApplyButton);

        commentApplyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View parent = (View)view.getParent();

                commentCardviewGuideText = parent.findViewById(R.id.commentCardviewGuideText);
                commentHeader = parent.findViewById(R.id.commentHeader);
                commentBody = parent.findViewById(R.id.commentBody);
                commentAuthor = parent.findViewById(R.id.commentAuthor);

                commentHeader.setText("");
                commentBody.setText("");
                commentAuthor.setText("");

                EditText commentNumberEditText = parent.findViewById(R.id.commentNumberEditText);
                String commentNumberString = commentNumberEditText.getText().toString();

                if (TextUtils.isEmpty(commentNumberString)) {
                    Toast.makeText(getActivity(), getString(R.string.enter_comment_number), Toast.LENGTH_SHORT).show();
                    commentCardviewGuideText.setVisibility(View.VISIBLE);
                    return;
                }

                int commentNumber = Integer.parseInt(commentNumberString);

                if (commentNumber <= 0 || commentNumber > 500){
                    Toast.makeText(getActivity(), getString(R.string.no_such_comment), Toast.LENGTH_SHORT).show();
                    commentCardviewGuideText.setVisibility(View.VISIBLE);
                    return;
                }

                if (InternetConnection.checkConnection(getContext())){

                    retrieveUserInfoFromApi(commentNumber);

                }else {
                    Toast.makeText(getActivity(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                    commentCardviewGuideText.setVisibility(View.VISIBLE);
                }

            }
        });

        return view;
    }

    private void retrieveUserInfoFromApi(int commentNumber) {

        ApiService api = RetroClient.getApiService();
        Call<Comment> call = api.getComment(commentNumber);

        /**
         * Enqueue Callback will be call when get response...
         */
        call.enqueue(new Callback<Comment>() {
            @Override
            public void onResponse(Call<Comment> call, Response<Comment> response) {
                if (response.isSuccessful()){
                    commentCardviewGuideText.setVisibility(View.INVISIBLE);
                    commentHeader.setText(response.body().getName());
                    commentBody.setText(response.body().getBody());
                    commentAuthor.setText(response.body().getEmail());
                }else {
                    commentCardviewGuideText.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(), getString(R.string.smth_went_wrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Comment> call, Throwable t) {

            }
        });

    }


    public CardView getCardView() {return cardView;}
}
