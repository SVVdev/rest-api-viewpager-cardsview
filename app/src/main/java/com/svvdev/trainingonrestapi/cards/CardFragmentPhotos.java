package com.svvdev.trainingonrestapi.cards;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.svvdev.trainingonrestapi.R;
import com.svvdev.trainingonrestapi.resclasses.Photo;
import com.svvdev.trainingonrestapi.resclasses.Post;
import com.svvdev.trainingonrestapi.utils.ApiService;
import com.svvdev.trainingonrestapi.utils.InternetConnection;
import com.svvdev.trainingonrestapi.utils.RetroClient;
import com.svvdev.trainingonrestapi.utils.adapters.CardAdapter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CardFragmentPhotos extends Fragment implements CardFragment {

    private CardView cardView;
    TextView photoCardviewGuideText;
    TextView photoDescription;
    ImageView photoView;
    private int photoNumber = 2; // #3 bad for the design

    public static Fragment getInstance(int position){
        CardFragmentPhotos f = new CardFragmentPhotos();
        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);

        return f;
    }

    public CardView getCardView() {return cardView;}


    @SuppressLint("DefaultLocale")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.item_viewpager_photos, container, false);

        cardView = (CardView) view.findViewById(R.id.photosCardView);
        cardView.setMaxCardElevation(cardView.getCardElevation() * CardAdapter.MAX_ELEVATION_FACTOR);

        Button photoApplyButton = (Button)view.findViewById(R.id.photosApplyButton);
        photoCardviewGuideText = view.findViewById(R.id.photoCardviewGuideText);
        photoDescription = view.findViewById(R.id.photoDescription);
        photoView = view.findViewById(R.id.photoView);



        photoApplyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View parent = (View)view.getParent();

                photoDescription.setText("");

                if (InternetConnection.checkConnection(getContext())){

                    retrieveUserInfoFromApi(parent.getContext());

                }else {
                    Toast.makeText(getActivity(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                    photoCardviewGuideText.setVisibility(View.VISIBLE);
                }

            }
        });

        return view;
    }

    private void retrieveUserInfoFromApi(final Context context) {

        ApiService api = RetroClient.getApiService();
        Call<Photo> call = api.getPhoto(photoNumber);

        /**
         * Enqueue Callback will be call when get response...
         */
        call.enqueue(new Callback<Photo>() {
            @Override
            public void onResponse(Call<Photo> call, Response<Photo> response) {
                if (response.isSuccessful()){
                    photoCardviewGuideText.setVisibility(View.INVISIBLE);
                    Picasso
                            .with(context)
                            .load(response.body().getUrl())
                            .into(photoView);
                    photoDescription.setText(response.body().getTitle());
                }else {
                    photoCardviewGuideText.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(), getString(R.string.smth_went_wrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Photo> call, Throwable t) {

            }
        });


    }
}
