package com.svvdev.trainingonrestapi.cards;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.svvdev.trainingonrestapi.utils.adapters.CardAdapter;
import com.svvdev.trainingonrestapi.R;
import com.svvdev.trainingonrestapi.resclasses.Post;
import com.svvdev.trainingonrestapi.utils.ApiService;
import com.svvdev.trainingonrestapi.utils.InternetConnection;
import com.svvdev.trainingonrestapi.utils.RetroClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CardFragmentPosts extends Fragment implements CardFragment {

    private CardView cardView;
    private TextView postCardviewGuideText;
    private TextView postHeader;
    private TextView postBody;


    public static Fragment getInstance(int position){
        CardFragmentPosts f = new CardFragmentPosts();
        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);

        return f;
    }

    @SuppressLint("DefaultLocale")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.item_viewpager_posts, container, false);

        cardView = (CardView) view.findViewById(R.id.postsCardView);
        cardView.setMaxCardElevation(cardView.getCardElevation() * CardAdapter.MAX_ELEVATION_FACTOR);

        Button postApplyButton = (Button)view.findViewById(R.id.postsApplyButton);

        postApplyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View parent = (View)view.getParent();

                postCardviewGuideText = parent.findViewById(R.id.postCardviewGuideText);
                postHeader = parent.findViewById(R.id.postHeader);
                postBody = parent.findViewById(R.id.postBody);

                postHeader.setText("");
                postBody.setText("");

                EditText postNumberEditText = parent.findViewById(R.id.postNumberEditText);
                String postNumberString = postNumberEditText.getText().toString();

                if (TextUtils.isEmpty(postNumberString)) {
                    Toast.makeText(getActivity(), getString(R.string.enter_post_number), Toast.LENGTH_SHORT).show();
                    postCardviewGuideText.setVisibility(View.VISIBLE);
                    return;
                }

                int postNumber = Integer.parseInt(postNumberString);

                if (postNumber <= 0 || postNumber > 100){
                    Toast.makeText(getActivity(), getString(R.string.no_such_post), Toast.LENGTH_SHORT).show();
                    postCardviewGuideText.setVisibility(View.VISIBLE);
                    return;
                }

                if (InternetConnection.checkConnection(getContext())){

                    retrieveUserInfoFromApi(postNumber);

                }else {
                    Toast.makeText(getActivity(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                    postCardviewGuideText.setVisibility(View.VISIBLE);
                }

            }
        });

        return view;
    }

    private void retrieveUserInfoFromApi(int postNumber) {

        ApiService api = RetroClient.getApiService();
        Call<Post> call = api.getPost(postNumber);

        /**
         * Enqueue Callback will be call when get response...
         */
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                if (response.isSuccessful()){
                    postCardviewGuideText.setVisibility(View.INVISIBLE);
                    postHeader.setText(response.body().getTitle());
                    postBody.setText(response.body().getBody());
                }else {
                    postCardviewGuideText.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(), getString(R.string.smth_went_wrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {

            }
        });

    }


    public CardView getCardView() {return cardView;}
}
