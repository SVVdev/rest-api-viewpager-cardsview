package com.svvdev.trainingonrestapi.cards;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.svvdev.trainingonrestapi.R;
import com.svvdev.trainingonrestapi.resclasses.Photo;
import com.svvdev.trainingonrestapi.resclasses.Todo;
import com.svvdev.trainingonrestapi.utils.ApiService;
import com.svvdev.trainingonrestapi.utils.InternetConnection;
import com.svvdev.trainingonrestapi.utils.RetroClient;
import com.svvdev.trainingonrestapi.utils.adapters.CardAdapter;

import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CardFragmentTodos extends Fragment implements CardFragment {

    private CardView cardView;
    TextView todoCardviewGuideText;
    TextView todosHeader;
    TextView todosBody;
    TextView todoStatus;
    ImageView todoView;

    public static Fragment getInstance(int position){
        CardFragmentTodos f = new CardFragmentTodos();
        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);

        return f;
    }

    public CardView getCardView() {return cardView;}


    @SuppressLint("DefaultLocale")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.item_viewpager_todos, container, false);

        cardView = (CardView) view.findViewById(R.id.todosCardView);
        cardView.setMaxCardElevation(cardView.getCardElevation() * CardAdapter.MAX_ELEVATION_FACTOR);

        Button todosApplyButton = (Button)view.findViewById(R.id.todosApplyButton);
        todoCardviewGuideText = view.findViewById(R.id.todosCardviewGuideText);
        todosHeader = view.findViewById(R.id.todosHeader);
        todosBody = view.findViewById(R.id.todosBody);
        todoStatus = view.findViewById(R.id.todoStatus);



        todosApplyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View parent = (View)view.getParent();

                todosHeader.setText("");
                todosBody.setText("");
                todoStatus.setText("");

                if (InternetConnection.checkConnection(getContext())){

                    retrieveUserInfoFromApi(parent.getContext());

                }else {
                    Toast.makeText(getActivity(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                    todoCardviewGuideText.setVisibility(View.VISIBLE);
                }

            }
        });

        return view;
    }

    private void retrieveUserInfoFromApi(final Context context) {

        ApiService api = RetroClient.getApiService();

        int min = 1;
        int max = 200;

        Random r = new Random();
        int randNumber = r.nextInt(max - min + 1) + min;

        Call<Todo> call = api.getTodo(randNumber);

        /**
         * Enqueue Callback will be call when get response...
         */
        call.enqueue(new Callback<Todo>() {
            @Override
            public void onResponse(Call<Todo> call, Response<Todo> response) {
                if (response.isSuccessful()){
                    todoCardviewGuideText.setVisibility(View.INVISIBLE);
                    todosHeader.setText(getString(R.string.task_number) + response.body().getId());
                    todosBody.setText(response.body().getTitle());
                    boolean completed = response.body().getCompleted();
                    if (completed){
                        todoStatus.setText(R.string.completed);
                        todoStatus.setTextColor(getResources().getColor(R.color.ready));
                    } else{
                        todoStatus.setText(R.string.in_progress);
                        todoStatus.setTextColor(getResources().getColor(R.color.warning));
                    }

                }else {
                    todoCardviewGuideText.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(), getString(R.string.smth_went_wrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Todo> call, Throwable t) {

            }
        });


    }
}
