package com.svvdev.trainingonrestapi.cards;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.svvdev.trainingonrestapi.utils.adapters.CardAdapter;
import com.svvdev.trainingonrestapi.R;
import com.svvdev.trainingonrestapi.resclasses.user.User;
import com.svvdev.trainingonrestapi.utils.ApiService;
import com.svvdev.trainingonrestapi.utils.InternetConnection;
import com.svvdev.trainingonrestapi.utils.RetroClient;
import com.svvdev.trainingonrestapi.utils.adapters.UsersListViewAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CardFragmentUsers extends Fragment implements CardFragment {

    private CardView cardView;
    volatile ArrayList<User> users = new ArrayList<User>();
    UsersListViewAdapter usersListViewAdapter;
    TextView userCardviewGuideText;
    int numOfUsersToShow = 5;

    public static Fragment getInstance(int position){
        CardFragmentUsers f = new CardFragmentUsers();
        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);

        return f;
    }

    public CardView getCardView() {return cardView;}

    @SuppressLint("DefaultLocale")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.item_viewpager_users, container, false);

        cardView = (CardView) view.findViewById(R.id.usersCardView);
        cardView.setMaxCardElevation(cardView.getCardElevation() * CardAdapter.MAX_ELEVATION_FACTOR);

        Button usersApplyButton = (Button)view.findViewById(R.id.usersApplyButton);

        userCardviewGuideText = view.findViewById(R.id.userCardviewGuideText);

        usersListViewAdapter = new UsersListViewAdapter(view.getContext(), users);
        ListView usersListView = (ListView) view.findViewById(R.id.usersListView);
        usersListView.setAdapter(usersListViewAdapter);


        usersApplyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View parent = (View)view.getParent();


                if (InternetConnection.checkConnection(getContext())){

                    retrieveUserInfoFromApi();

                }else {
                    Toast.makeText(getActivity(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                    userCardviewGuideText.setVisibility(View.VISIBLE);
                }

            }
        });

        return view;
    }

    private void retrieveUserInfoFromApi() {

        ApiService api = RetroClient.getApiService();

        // clear old users
        users.clear();
        usersListViewAdapter.notifyDataSetChanged();

            // getting and filling data
            Call<List<User>> call = api.getUser();
            /**
             * Enqueue Callback will be call when get response...
             */
            call.enqueue(new Callback<List<User>>() {
                @Override
                public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                    if (response.isSuccessful()){
                        userCardviewGuideText.setVisibility(View.INVISIBLE);

                        int counter = 0;
                        for (User user : response.body()){
                            if (counter == numOfUsersToShow)
                                break;

                            user.setUsername("@" + user.getUsername());
                            users.add(user);

                            counter++;
                        }

                        // Notify the adapter that the data have changed
                        usersListViewAdapter.notifyDataSetChanged();
                    }else {
                        userCardviewGuideText.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(), getString(R.string.smth_went_wrong), Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                @Override
                public void onFailure(Call<List<User>> call, Throwable t) {

                }
            });

    }

}
