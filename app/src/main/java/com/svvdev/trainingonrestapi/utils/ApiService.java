package com.svvdev.trainingonrestapi.utils;


import com.svvdev.trainingonrestapi.resclasses.Comment;
import com.svvdev.trainingonrestapi.resclasses.Photo;
import com.svvdev.trainingonrestapi.resclasses.Post;
import com.svvdev.trainingonrestapi.resclasses.Todo;
import com.svvdev.trainingonrestapi.resclasses.user.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiService {

    @GET("/posts/{id}")
    Call<Post> getPost(@Path("id") int id);
    @GET("/comments/{id}")
    Call<Comment> getComment(@Path("id") int id);
    @GET("/users")
    Call<List<User>> getUser();
    @GET("/photos/{id}")
    Call<Photo> getPhoto(@Path("id") int id);
    @GET("/todos/{id}")
    Call<Todo> getTodo(@Path("id") int id);

}
