package com.svvdev.trainingonrestapi.utils.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.widget.CardView;
import android.view.ViewGroup;

import com.svvdev.trainingonrestapi.cards.CardFragment;
import com.svvdev.trainingonrestapi.cards.CardFragmentComments;
import com.svvdev.trainingonrestapi.cards.CardFragmentPhotos;
import com.svvdev.trainingonrestapi.cards.CardFragmentPosts;
import com.svvdev.trainingonrestapi.cards.CardFragmentTodos;
import com.svvdev.trainingonrestapi.cards.CardFragmentUsers;

import java.util.ArrayList;
import java.util.List;


public class CardFragmentPagerAdapter extends FragmentStatePagerAdapter implements CardAdapter {

    private List<CardFragment> fragments;
    private float baseElevation;

    private void addCardFragment(CardFragment fragment) {
        fragments.add(fragment);
    }

    public CardFragmentPagerAdapter(FragmentManager fm, float baseElevation){
        super(fm);
        fragments = new ArrayList<>();
        this.baseElevation = baseElevation;

        // first card
        addCardFragment(new CardFragmentPosts());
        // second card
        addCardFragment(new CardFragmentComments());
        // third card
        addCardFragment(new CardFragmentUsers());
        // fourth card
        addCardFragment(new CardFragmentPhotos());
        // fifth card
        addCardFragment(new CardFragmentTodos());
    }

    @Override
    public float getBaseElevation() {
        return baseElevation;
    }

    @Override
    public CardView getCardViewAt(int position) {
        return fragments.get(position).getCardView();
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0: return CardFragmentPosts.getInstance(position);
            case 1: return CardFragmentComments.getInstance(position);
            case 2: return CardFragmentUsers.getInstance(position);
            case 3: return CardFragmentPhotos.getInstance(position);
            case 4: return CardFragmentTodos.getInstance(position);
            default: return null;
        }
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Object fragment = super.instantiateItem(container, position);
        fragments.set(position, (CardFragment)fragment);
        return fragment;
    }
}
