package com.svvdev.trainingonrestapi.utils.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.svvdev.trainingonrestapi.R;
import com.svvdev.trainingonrestapi.resclasses.user.User;

import java.util.ArrayList;


public class UsersListViewAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<User> users;

    public UsersListViewAdapter(Context context, ArrayList<User> users){
        this.context = context;
        this.users = users;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int i) {
        return users.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // используем созданные, но не используемые view
        View view = convertView;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.list_view_item_users, parent, false);
        }

        User user = getUser(position);

        // заполняем View в пункте списка данными из товаров: наименование, цена
        // и картинка
        ((TextView) view.findViewById(R.id.name)).setText(user.getName());
        ((TextView) view.findViewById(R.id.username)).setText(user.getUsername());
        ((TextView) view.findViewById(R.id.city)).setText(user.getAddress().getCity());
        ((TextView) view.findViewById(R.id.email)).setText(user.getEmail());
        ((TextView) view.findViewById(R.id.phone)).setText(user.getPhone());

        return view;
    }

    // товар по позиции
    User getUser(int position) {
        return ((User) getItem(position));
    }

}
